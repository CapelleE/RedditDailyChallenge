/*
 * https://www.reddit.com/r/dailyprogrammer/comments/3pcb3i/20151019_challenge_237_easy_broken_keyboard/
 */

#include <chrono>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <Windows.h>

using namespace std;

FILE *handle = nullptr;

const string readWord(FILE *handle);
void resetFilePtr(FILE *handle);
bool checkWordWritable(const string& word, const string& writableChars);
void sortWritableCharacters(string& wc);
int compareCharacters(char a, char b);

BOOL CtrlHandler(DWORD fwdCtrlType)
{
	switch (fwdCtrlType)
	{
	case CTRL_C_EVENT:
	case CTRL_CLOSE_EVENT:
		printf("Closing app securely.");
		if (handle != nullptr)
			fclose(handle);
		return FALSE;
	default:
		return FALSE;
	}
}

int main(void)
{
	const char* filename = "en-dictionary.txt";
	string longestWritableWord = "";
	string writableChars = "";
	// Defines the "maximum" word starting with this letter
	// Example: writableChars = "abc" -> max writable char = "c" -> only search words beginning with 'a' through 'c', no need to go further.
	char maxWritableChar = 0;
	
	cout << "Input the writable characters: ";
	cin >> writableChars;
	cin.get();

	sortWritableCharacters(writableChars);
	maxWritableChar = writableChars[writableChars.length() - 1];
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE);

	if ((handle = fopen(filename, "r")) != nullptr)
	{
		if (writableChars.length() > 0)
		{
			cout << "Dictionary file opened." << endl << endl;
			resetFilePtr(handle);

			string w;

			while (true)
			{
				//TODO: find a better way to do this like dichotomic search or remove possibilities as soft goes.
				// Cannot load the whole file in the ram though (too big, would take a lot of time)
				w = readWord(handle);
				// Stop conditions :
				// word is empty (eof)
				// max writable character is passed
				if (w.length() <= 0
					|| compareCharacters(w[0], maxWritableChar) > 0)
					break;
				// cout << w << endl;
				bool isWritable = checkWordWritable(w, writableChars);
				if(w.length() > longestWritableWord.length() && isWritable)
					longestWritableWord = w; // copy
			}
		}
		fclose(handle);
	}
	else
	{
		cout << "Cannot open file: " << filename << endl;
	}

	cout << "The longest writable word with your broken keyboard is: " << longestWritableWord << endl;
	cin.get();

	return 0;
}

const string readWord(FILE *handle)
{
	string result = "";
	char buffer;
	int freadResult = 0;
	
	while(1)
	{
		freadResult = fread(&buffer, 1, 1, handle);
		if (freadResult == EOF)
			break;
		else if (buffer == '\n' || buffer == '\0')
			break;
		else
			result += buffer;
	}
	
	return result;
}

void resetFilePtr(FILE *handle)
{
	rewind(handle);
}

int compareCharacters(char a, char b)
{
	return a - b;
}

void sortWritableCharacters(string& wc)
{
	cout << "Before sorting: " << wc << endl;
	int pass = 0;
	int comparisonValue = 0;
	while (pass < wc.length())
	{
		for (int x = 0; x < wc.length(); ++x)
		{
			comparisonValue = compareCharacters(wc[x], wc[x + 1]);
			if (comparisonValue > 0 && wc[x+1] != '\0')
			{
				char temp = wc[x];
				wc[x] = wc[x + 1];
				wc[x + 1] = temp;
			}
			if (comparisonValue == 0)
				wc.erase(wc.begin() + x);
		}
		++pass;
	}
	cout << "After sorting: " << wc << endl;
}

bool checkWordWritable(const string& word, const string& writableChars)
{
	//TODO: find a better way to do this..
	bool ok;
	for (int wx = 0; wx < word.length(); ++wx)
	{
		ok = false;
		for (int wcx = 0; wcx < writableChars.length(); ++wcx)
		{
			if (compareCharacters(word[wx], writableChars[wcx]) == 0)
				ok = true;
		}
		if (!ok) return false;
	}
	
	return true;
}